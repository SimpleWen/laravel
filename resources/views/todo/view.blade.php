
<button  onclick="history.back(-1);"> 返回上一頁</button>
<table>
    <thead>
        <tr>
            <td>編號</td>
            <td>標題</td>
            <td>建立時間</td>
            <td>更改時間</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $aTodo['id'] }}</td>
            <td>{{ $aTodo['title'] }}</td>
            <td>{{ date('Y-m-d h:m:s', $aTodo['created_at']) }}</td>
            <td>{{ date('Y-m-d h:m:s', $aTodo['updated_at']) }}</td>
        </tr>
    </tbody>
</table>


