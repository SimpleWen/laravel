@if ($errors->has('title'))
    <strong>{{ $errors->first('title') }}</strong>
@endif

@foreach ($todos as $todo)
    <p>
        {{ $todo->id . '.' . $todo->title }}
        <form action="{{ route("todo_delete", $todo['id']) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">
        </form>
        <a href="{{ route('todo_view', $todo['id'] ) }}">檢視按鈕</a>
        <a href="{{ route('todo_edit', $todo['id'] ) }}">編輯按鈕</a>
    </p>
@endforeach
<form action="{{ url('todo') }}" method="POST">
    {{ csrf_field() }}
    <input type="text" placeholder="請輸入東西" name="title">
    <input type="submit">
</form>