
<button  onclick="history.back(-1);"> 返回上一頁</button>
<h1>這個是form1 method Get的表單</h1>
<form action="{{ route('todo_get_submit', $aTodo['id'] ) }}" method="GET">
    <div>
        <label for="">Todo ID</label>
        <input type="text" readonly value="{{ $aTodo['id'] }}" name="id">

    </div>
    <div>
        <label for="">Todo Title</label>
        <input type="text" value="{{ $aTodo['title'] }}" name="title">

    </div>
    <div>

        <label for="">Todo Created Time</label>
        <input type="text" readonly value="{{ $aTodo['created_at'] }}" name="created_time">
    </div>
    <div>
        <label for="">Todo Updated Time</label>
        <input type="text" readonly value="{{ $aTodo['updated_at'] }}" name="update_time">
    </div>
    <input type="submit">
</form>



