<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/todo', 'TodoController@index')->name('index_home');
Route::post('/todo', 'TodoController@update');
Route::delete('/todo/{id}', 'TodoController@destroy')->name('todo_delete');

Route::get('/todo/view/{id}', 'TodoController@view')->name('todo_view');
Route::get('/todo/edit/{id}', 'TodoController@edit')->name('todo_edit');

Route::get('todo/get/submit/{id}', 'TodoController@getSubmit')->name('todo_get_submit');
Route::post('todo/post/submit/{id}', 'TodoController@postSubmit')->name('todo_post_submit');


//透過 get 攜帶很多參數，而非只有{id} 這種單一個
Route::get('todo/get/params', 'TodoController@getParam')->name('params');
Route::get('testing', 'TodoController@ParamTestView');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




// 先暖身一下
Route::get('alltop', function (){
    return '<h1>Hello World!!</h1>' . "<br>" . "<h2>沒錯! 就是這麼簡單</h2>";
});

Route::get('alltopController', 'testController@ThisIsMethod');



//如果是下方的 url 還想攜帶 get 路徑的話，需要先給id， 一樣使用 route('params', '100?School=name&something=sad&abc=ccc');
Route::get('todo/get/params/{id}', 'TodoController@getParam')->name('params');