<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TodoController extends Controller
{
    public function index()
    {
        //$todos = DB::connection('sqlsrv')->table('todos')->get();
        $todos = Todo::all();
        // dd($todos);

        return view('todo.index', [
            'todos' => $todos
        ]);
    }
    public function update(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|min:3'
        ]);
        $todo = Todo::create($validated);
        return redirect('todo');
        // $todo = new Todo();
        // $todo->title = $request->title;
        // $todo->save();

        // $todo = Todo::create([
        //     'title' => $request->title,
        // ]);

    }
    public function destroy(Request $request,  $id)
    {
        Todo::find($id)->delete();
        
        return redirect('todo');
    }

    public function view(Request $request, $id){
        $aTodo = Todo::find($id)->toArray();
        return view('todo.view', array('aTodo' => $aTodo));
    }

    public function edit(Request $request, $id){
        $aTodo = Todo::find($id)->toArray();
        return view('todo.get_edit', array('aTodo' => $aTodo));
    }

    public function getSubmit(Request $request, $id){
        //return redirect()->route('index_home');
        
        $result = Todo::updateOrCreate(array('id' => $id), $request->all());
        //dd($result);
        return redirect()->route('todo_view', $result->id);
        dd($request);
    }


    //測試 route() 攜帶 get 參數
    public function ParamTestView(Request $request){
        return view('test.test');
    }

    public function getParam(Request $request){
        dd($request);
    }
}
